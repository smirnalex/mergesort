package cft.mergeSort.factory;

import cft.mergeSort.comparator.MyComparator;

public interface ComparatorFactory{
    MyComparator createComparator();
}
