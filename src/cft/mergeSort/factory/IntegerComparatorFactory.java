package cft.mergeSort.factory;

import cft.mergeSort.comparator.IntegerComparator;

public class IntegerComparatorFactory implements ComparatorFactory{
    @Override
    public IntegerComparator createComparator(){
        return new IntegerComparator();
    }
}
