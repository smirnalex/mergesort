package cft.mergeSort.factory;

import cft.mergeSort.comparator.StringComparator;

public class StringComparatorFactory implements ComparatorFactory{
    @Override
    public StringComparator createComparator(){
        return new StringComparator();
    }
}
