package cft.mergeSort;

import cft.mergeSort.argsResolver.ArgumentsResolver;
import cft.mergeSort.comparator.MyComparator;
import cft.mergeSort.sorter.Sorter;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
	    try{
            ArgumentsResolver resolver = new ArgumentsResolver(args);
            MyComparator comparator = resolver.resolve();
            Sorter sorter = new Sorter(resolver.getFiles(), comparator);
            sorter.sort();
        }
	    catch(IOException exc){
            System.out.println(exc.getMessage());
        }
    }
}