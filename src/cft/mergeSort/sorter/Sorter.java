package cft.mergeSort.sorter;

import cft.mergeSort.comparator.MyComparator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Map;
import java.util.List;
import java.util.Iterator;

public class Sorter {

    enum ReadResult {SUCCESS, FAIL, EOF}

    private Map<BufferedReader,String> readers;
    private BufferedWriter writer;
    private MyComparator comparator;

    public Sorter(List<String> files, MyComparator comparator) throws IOException{
        FilesOpener filesOpener = new FilesOpener(files);
        readers = filesOpener.openInputFiles();
        writer = filesOpener.openOutputFile();
        this.comparator = comparator;
    }

    public void sort() throws IOException{
        startingValues();
        sorting();
        try{
            writer.close();
        }
        catch (IOException exc){
            throw new IOException("Failed to close output file!");
        }
    }

    private void sorting(){
        if(readers.size() == 0){
            return;
        }
        Iterator<Map.Entry<BufferedReader,String>> itr = readers.entrySet().iterator();
        Map.Entry<BufferedReader, String> bestPair = itr.next();
        while(itr.hasNext()) {
            Map.Entry<BufferedReader, String> pair = itr.next();
            if(comparator.compare(pair.getValue(), bestPair.getValue()) > 0){
                bestPair = pair;
            }
        }
        try{
             writer.write(bestPair.getValue());
             writer.newLine();
        }
        catch (IOException exc){
            System.out.println("Failed to write a line!");
        }
        ReadResult result = ReadResult.FAIL;
        while(result == ReadResult.FAIL) {
            try {
                result = readLine(bestPair.getKey());
                if(result == ReadResult.EOF){
                    bestPair.getKey().close();
                    readers.remove(bestPair.getKey());
                }
            } catch (IOException exc) {
                System.out.println(exc.getMessage());
            }
        }
        sorting();
    }

    private void startingValues(){
        Iterator<Map.Entry<BufferedReader,String>> itr = readers.entrySet().iterator();
        while(itr.hasNext()){
            Map.Entry<BufferedReader,String> pair = itr.next();
            ReadResult result = ReadResult.FAIL;
            while(result == ReadResult.FAIL) {
                try {
                    result = readLine(pair.getKey());
                    if(result == ReadResult.EOF){
                        try{
                            pair.getKey().close();
                        }
                        catch (IOException exc){
                            throw new IOException("Failed to close input file!");
                        }
                        itr.remove();
                    }
                }
                catch (IOException exc){
                    System.out.println(exc.getMessage());
                }
            }
        }
    }

    private ReadResult readLine(BufferedReader reader) throws IOException{
        String prevLine = readers.get(reader);
        String newLine;
        try {
            newLine = reader.readLine();
        }
        catch(IOException exc){
            throw new IOException("Failed to read a line!");
        }
        if(newLine == null){
            return ReadResult.EOF;
        }
        else if(!comparator.isCorrect(newLine)){
            throw new IOException(newLine + " has incorrect type!");
        }
        else if(prevLine != null && comparator.compare(prevLine,newLine) < 0){
            throw new IOException(prevLine + " and "  + newLine + " have wrong order!");
        }
        else{
            readers.put(reader,newLine);
            return ReadResult.SUCCESS;
        }
    }
}
