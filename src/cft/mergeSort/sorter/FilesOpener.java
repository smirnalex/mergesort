package cft.mergeSort.sorter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class FilesOpener {

    private List<String> files;

    FilesOpener(List<String> files){
        this.files = files;
    }

    BufferedWriter openOutputFile() throws IOException {
        try{
            return Files.newBufferedWriter(Paths.get(files.get(0)), StandardCharsets.UTF_8);
        }
        catch(IOException exc){
            throw new IOException("Can not open output file!");
        }
    }

    Map<BufferedReader,String> openInputFiles() throws IOException {
        Map<BufferedReader,String> readers = new HashMap<>();
        for(int i = 1; i < files.size(); i++){
            try{
                BufferedReader reader = Files. newBufferedReader(Paths.get(files.get(i)), StandardCharsets.UTF_8);
                readers.put(reader,null);
            }
            catch (IOException exc){
                System.out.println("Can not open input file: " + files.get(i));
            }
        }
        if(readers.size() < 1){
            throw new IOException("Can not open at least one input file!");
        }
        return readers;
    }
}