package cft.mergeSort.comparator;

public abstract class MyComparator{
    enum Order {ASCENDING, DESCENDING}

    Order order = Order.ASCENDING;

    public void setDescending() {
        order = Order.DESCENDING;
    }

    public abstract boolean isCorrect(String str);

    public abstract int compare(String a, String b);
}
