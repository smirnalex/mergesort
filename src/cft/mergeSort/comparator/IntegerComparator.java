package cft.mergeSort.comparator;

public class IntegerComparator extends MyComparator{

    @Override
    public int compare(String str1, String str2){
        Integer a = Integer.parseInt(str1);
        Integer b = Integer.parseInt(str2);
        if(order == Order.ASCENDING){
            return b.compareTo(a);
        }
        else{
            return a.compareTo(b);
        }
    }

    @Override
    public boolean isCorrect(String str){
        try{
            Integer.parseInt(str);
            return true;
        }
        catch (NumberFormatException exc){
            return false;
        }
    }
}
