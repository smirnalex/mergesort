package cft.mergeSort.argsResolver;

import cft.mergeSort.factory.ComparatorFactory;
import cft.mergeSort.factory.IntegerComparatorFactory;
import cft.mergeSort.factory.StringComparatorFactory;

import java.util.Map;

class Filler {
    private static final String STRING = "-s";
    private static final String INT = "-i";
    static final String ASCENDING = "-a";
    static final String DESCENDING = "-d";

    void fillPossibleArgs(Map<String, Mention> possibleArgs){
        possibleArgs.put(STRING, Mention.NOT_FOUND);
        possibleArgs.put(INT, Mention.NOT_FOUND);
        possibleArgs.put(ASCENDING, Mention.NOT_FOUND);
        possibleArgs.put(DESCENDING, Mention.NOT_FOUND);
    }

    void fillPossibleTypeArgs(Map<String, ComparatorFactory> possibleTypeArgs){
        possibleTypeArgs.put(STRING, new StringComparatorFactory());
        possibleTypeArgs.put(INT, new IntegerComparatorFactory());

    }
}
