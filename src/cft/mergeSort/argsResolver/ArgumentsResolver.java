package cft.mergeSort.argsResolver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cft.mergeSort.comparator.MyComparator;
import cft.mergeSort.factory.ComparatorFactory;

import static cft.mergeSort.argsResolver.Filler.ASCENDING;
import static cft.mergeSort.argsResolver.Filler.DESCENDING;

public class ArgumentsResolver {

    private final Map<String, ComparatorFactory> possibleTypeArgs= new HashMap<>();
    private final Map<String,Mention> possibleArgs = new HashMap<>();
    private List<String> files = new ArrayList<>();
    private String[] args;

    public ArgumentsResolver(String[] args) {
        this.args = args;
        Filler filler = new Filler();
        filler.fillPossibleArgs(possibleArgs);
        filler.fillPossibleTypeArgs(possibleTypeArgs);
    }

    public List<String> getFiles(){
        return files;
    }

    public MyComparator resolve() throws IOException{
        MyComparator comparator = null;
        for(String str : args){
            if (possibleTypeArgs.containsKey(str)) {
                if(comparator == null) {
                    comparator = possibleTypeArgs.get(str).createComparator();
                    possibleArgs.put(str,Mention.FOUND);
                }
                else if(possibleArgs.get(str) == Mention.NOT_FOUND){
                    throw new IOException("Specify only one type of data!");
                }
            }
            else if (possibleArgs.containsKey(str)) {
                possibleArgs.put(str,Mention.FOUND);
            }
            else{
                files.add(str);
            }
        }

        if(files.size() < 2){
            throw new IOException("Specify at least one input and one output file!");
        }
        if(comparator == null){
            throw new IOException("Item type not specified!");
        }
        if(possibleArgs.get(ASCENDING) == Mention.FOUND && possibleArgs.get(DESCENDING) == Mention.FOUND){
            throw new IOException("Specify only one sort order!");
        }
        if(possibleArgs.get(DESCENDING) == Mention.FOUND){
            comparator.setDescending();
        }

        return comparator;
    }
}

